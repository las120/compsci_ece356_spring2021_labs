This is the git repo for all the labs of COMPSCI/ECE 356. Please checkout different branches for different labs.


Lab Echo Server: branch echo_server  
Lab Reliable Transport: branch reliable_transport  
Lab Congestion Control: branch congestion_control  
Lab Static Routing: branch static_routing  
Lab Dynamic Routing: branch dynamic_routing  

# COMPSCI 356 Computer Network Architecture: Lab 1 Echo Server

This lab is a warm-up for the following four labs. You are required to implement a simple echo server individually using socket API. An echo server can listen on a specific TCP port, and wait for a client to connect. After a TCP connection is established between the echo server and a client, the client can send arbitrary messages to the server and the server will echo back the same message to the client.

For more information, please refer: https://www2.cs.duke.edu/courses/spring21/compsci356/labs/lab1/lab1.html

